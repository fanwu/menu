//
//  FWAppDelegate.h
//  mega_menu
//
//  Created by 范武 on 13-4-7.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FWViewController;
@class CitySwitcher;
@interface FWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FWViewController *viewController;

@property (strong, nonatomic) CitySwitcher * switcherViewController;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
