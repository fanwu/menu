//
//  CitySwitcher.h
//  mega_menu
//
//  Created by 范武 on 13-4-7.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitySwitcher : UIViewController
- (void)switchToCity:(NSString *)cityName;
@property (weak, nonatomic) UINavigationController * navigationController;
@end
