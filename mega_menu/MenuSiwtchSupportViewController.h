//
//  MenuSiwtchSupportViewController.h
//  mega_menu
//
//  Created by 范武 on 13-4-8.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CitySwitcher;
@interface MenuSiwtchSupportViewController : UIViewController
@property (strong, nonatomic) CitySwitcher * menuViewController;
@end
