//
//  MenuSiwtchSupportViewController.m
//  mega_menu
//
//  Created by 范武 on 13-4-8.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import "MenuSiwtchSupportViewController.h"
#import "CitySwitcher.h"
@interface MenuSiwtchSupportViewController ()
{
    @protected
    CGFloat menuHeight;
    UIButton * titleButton;
    UILabel * menuLabel;
    BOOL isMenuOnHidden;
}
- (void)toggleMenu:(id)sender;
@end

@implementation MenuSiwtchSupportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _menuViewController = [[CitySwitcher alloc]initWithNibName:@"CitySwitcher" bundle:nil];
        [self addChildViewController:_menuViewController];
        _menuViewController.navigationController = self.navigationController;
        menuHeight = CGRectGetHeight(_menuViewController.view.frame);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    

    
    DLog(@"menu high: %f",menuHeight);
    
    [_menuViewController.view.po_frameBuilder moveWithOffsetY:menuHeight * -1];

    isMenuOnHidden = YES;
    [self.view addSubview:_menuViewController.view];

    
    titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleButton setBackgroundImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [titleButton setFrame:CGRectMake(90, 12, 20, 20)];
    [titleButton addTarget:self action:@selector(toggleMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    menuLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 45)];
    menuLabel.textAlignment = NSTextAlignmentCenter;
    menuLabel.text = self.navigationItem.title;
    menuLabel.font = [UIFont boldSystemFontOfSize:20];
    menuLabel.textColor = [UIColor whiteColor];
    menuLabel.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer * touch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toggleMenu:)];
    [touch setNumberOfTapsRequired:1];
    [touch setNumberOfTouchesRequired:1];
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 130, 45)];
    [view addSubview:menuLabel];
    [view addSubview:titleButton];
    
    self.navigationItem.titleView = view;
    [self.navigationItem.titleView addGestureRecognizer:touch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleMenu:(id)sender
{
    if (isMenuOnHidden) {
        [self.view bringSubviewToFront:_menuViewController.view];
        [UIView animateWithDuration:0.5 animations:^{
            [_menuViewController.view.po_frameBuilder moveWithOffsetY:menuHeight];
            CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI);
            titleButton.transform = rotate;
        } completion:^(BOOL finished) {
            isMenuOnHidden = NO;
        }];
    }else
    {
        isMenuOnHidden = YES;
        [UIView animateWithDuration:0.5 animations:^{
            [_menuViewController.view.po_frameBuilder moveWithOffsetY: (-1 * menuHeight)];
            CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI * 2);
            titleButton.transform = rotate;
        } completion:^(BOOL finished) {
            [self.view sendSubviewToBack:_menuViewController.view];
            isMenuOnHidden = YES;
        }];
    }    
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    menuLabel.text = title;
}

@end
