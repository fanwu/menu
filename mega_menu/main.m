//
//  main.m
//  mega_menu
//
//  Created by 范武 on 13-4-7.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FWAppDelegate class]));
    }
}
