//
//  CitySwitcher.m
//  mega_menu
//
//  Created by 范武 on 13-4-7.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import "CitySwitcher.h"

@interface CitySwitcher ()
- (void)setAction;
- (void)btnOnClick:(id)sender;
@end

@implementation CitySwitcher

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setAction];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}


- (void)setAction
{
    for (NSInteger i = 1; i <= 10; ++i) {
        UIButton * city_btn = (UIButton *)[self.view viewWithTag:i];
        [city_btn addTarget:self action:@selector(btnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)btnOnClick:(id)sender
{
    UIButton * btn = sender;
    NSString * title = [btn titleForState:UIControlStateNormal];
    [self switchToCity:title];

}



- (void)switchToCity:(NSString *)cityName
{
    NSLog(@"City switch to :%@", cityName);
}

@end
