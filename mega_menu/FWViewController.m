//
//  FWViewController.m
//  mega_menu
//
//  Created by 范武 on 13-4-7.
//  Copyright (c) 2013年 福建邮科通讯服务有限公司. All rights reserved.
//

#import "FWViewController.h"

@interface FWViewController ()

@end

@implementation FWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setTitle:@"福建"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
